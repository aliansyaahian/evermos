/*
Navicat MySQL Data Transfer

Source Server         : rootMysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : evermos

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-07-08 21:53:56
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `magazine`
-- ----------------------------
DROP TABLE IF EXISTS `magazine`;
CREATE TABLE `magazine` (
  `magazine_type` varchar(50) NOT NULL,
  `magazine_name` varchar(50) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `bullets` int(11) DEFAULT NULL,
  PRIMARY KEY (`magazine_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of magazine
-- ----------------------------
INSERT INTO `magazine` VALUES ('mg1', 'Magazine 1', '10', '7');
INSERT INTO `magazine` VALUES ('mg2', 'Magazine 2', '2', '0');
INSERT INTO `magazine` VALUES ('mg3', 'Magazine 3', '5', '3');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'Samsung S10', '5');
INSERT INTO `product` VALUES ('2', 'Iphone 11', '7');
INSERT INTO `product` VALUES ('3', 'Iphone X', '3');
INSERT INTO `product` VALUES ('4', 'Samsung S20', '2');

-- ----------------------------
-- Table structure for `transaction`
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `trans_num` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`trans_num`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaction
-- ----------------------------
INSERT INTO `transaction` VALUES ('22', '1', 'aliansyah');
INSERT INTO `transaction` VALUES ('23', '3', 'ujang');
INSERT INTO `transaction` VALUES ('24', '2', 'aliansyah');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('aliansyah', 'Muhammad Aliansyah');
INSERT INTO `user` VALUES ('ujang', 'Ujang Soleh');
