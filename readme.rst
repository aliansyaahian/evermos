************
Installation
************

1. Clone this project
2. Run "composer install"
3. Execute sql file "evermos.sql" into your MySQL database

*****************
Basic GET example
*****************

Here is a basic example. This controller, which should be saved as `Transaction.php`, can be called in two ways:

* `http://domain/Transaction` will return the list of all transaction
* `http://domain/Transaction/id/22` will only return information about the transaction with id = 22
