<?php

class Transaction_model extends CI_Model{

	public $tb_product;
	public $tb_transaction;

	public $aResult = array(
		'is_error' => false,
		'error_message' => ''
	);

	public function __construct(){
		parent::__construct();

		$this->load->library('session');
		$this->db = $this->load->database('default', true);

		$this->tb_product = $this->db;
		$this->tb_transaction = $this->db;
	}

	public function getTransaction($id = null){
		if($id === null){
			return $this->db->get('transaction')->result_array();
		}else{
			return $this->db->get_where('transaction', ['trans_num' => $id])->result_array();
		}
	}

	public function createTransaction($data){
		// Delay execution in microseconds 1 until 3 seconds (random)
		usleep(rand(1000000,3000000));

		// Cek jumlah stok barang
		$checkStock = $this->checkStock($data['product_id']);
		
		if($checkStock['stock'] > 0){
			// Transaksi ke tabel "transaction"
			$this->tb_transaction->trans_begin();

			$this->tb_transaction->set('product_id', $data['product_id']);
			$this->tb_transaction->set('username', $data['username']);
			$this->tb_transaction->insert('transaction');

			if($this->tb_transaction->trans_status() === FALSE){
				// Jika transaksi gagal, rollback
				$this->tb_transaction->trans_rollback();
				$this->aResult['is_error'] = true;
				$this->aResult['error_message'] = "Terjadi kesalahan saat insert transaction";
			}else{
				// Transaksi ke tabel "product"
				$this->tb_product->trans_begin();

				$this->tb_product->set('stock', $checkStock['stock'] - 1);
				$this->tb_product->where('product_id', $data['product_id']);
				$this->tb_product->update('product');

				if($this->tb_product->trans_status() === FALSE){
					// Jika transaksi tabel product gagal, rollback
					$this->tb_transaction->trans_rollback();
					$this->tb_product->trans_rollback();

					$this->aResult['is_error'] = true;
					$this->aResult['error_message'] = "Terjadi kesalahan saat update product";
				}else{
					// Cek kembali jumlah stok barang
					$checkStockAgain = $this->checkStock($data['product_id']);
					if($checkStockAgain['stock'] < 0){						
						$this->tb_transaction->trans_rollback();
						$this->tb_product->trans_rollback();

						$this->aResult['is_error'] = true;
						$this->aResult['error_message'] = "Stok barang sudah habis";
					}else{
						// Transaksi sukses
						$this->tb_transaction->trans_commit();
						$this->tb_product->trans_commit();

						$this->aResult['is_error'] = false;
					}
				}
			}
		}else{
			$this->aResult['is_error'] = true;
			$this->aResult['error_message'] = "Stok barang sudah habis";
		}

		return $this->aResult;
	}

	public function checkStock($product_id){
		$this->db->select('product_id, product_name, stock');
		$this->db->from('product');
		$this->db->where('product_id', $product_id);
		return $this->db->get()->row_array();
	}
}