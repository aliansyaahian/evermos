<?php

class Product_model extends CI_Model{

	public function __construct(){
		parent::__construct();

		$this->load->library('session');
		$this->load->database('default', true);
	}

	public function getProduct($id = null){
		if($id === null){
			return $this->db->get('product')->result_array();
		}else{
			return $this->db->get_where('product', ['product_id' => $id])->result_array();
		}
	}
}