<?php

class Magazine_model extends CI_Model{

	public $aResult = array(
		'is_error' => false,
		'error_message' => '',
		'sta_full' => ''
	);

	public function __construct(){
		parent::__construct();

		$this->load->library('session');
		$this->db = $this->load->database('default', true);
	}

	public function getMagazine($id = null){
		if($id === null){
			return $this->db->get('magazine')->result_array();
		}else{
			return $this->db->get_where('magazine', ['magazine_type' => $id])->result_array();
		}
	}

	public function shootOnce($mg_type = null){
		$magazine = $this->checkMagazine($mg_type);
		if($magazine['bullets'] > 0){
			// Jika magazine full, beri pesan
			if($magazine['capacity'] == $magazine['bullets']){
				$this->aResult['sta_full'] = "Magazine full";
			}

			$this->shoot($magazine['bullets'], $mg_type);			
			$this->aResult['is_error'] = false;
			$this->aResult['error_message'] = "";
		}else{
			// peluru habis
			$this->aResult['is_error'] = true;
			$this->aResult['error_message'] = "Peluru habis";
		}
		return $this->aResult;
	}

	public function checkMagazine($mg_type){
		$this->db->select('magazine_type, magazine_name, capacity, bullets');
		$this->db->from('magazine');
		$this->db->where('magazine_type', $mg_type);
		return $this->db->get()->row_array();
	}

	public function shoot($bullets = null, $mg_type = null){
		$this->db->set('bullets', $bullets - 1);
		$this->db->where('magazine_type', $mg_type);
		$this->db->update('magazine');
	}

	public function loadBullets($bullets = null, $mg_type = null){
		$this->db->set('bullets', $bullets + 1);
		$this->db->where('magazine_type', $mg_type);
		$this->db->update('magazine');
	}

	public function autoload($mg_type = null){
		$tembak = false;
		while($tembak == false){
			// Periksa magazine
			$magazine = $this->checkMagazine($mg_type);

			// Magazine full
			if($magazine['capacity'] == $magazine['bullets']){
				// Bisa nembak
				$tembak = true;
				$this->shoot($magazine['bullets'], $mg_type);		// Test nembak

				$this->aResult['sta_full'] = "Magazine full";		// Verified magazine full
				$this->aResult['is_error'] = false;
				$this->aResult['error_message'] = "";
			}else{
				// Magazine belum full, isi kembali magazine
				$this->loadBullets($magazine['bullets'], $mg_type);
			}	
		}

		return $this->aResult;
	}

}