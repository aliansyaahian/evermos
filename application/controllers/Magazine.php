<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Magazine extends RestController {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();

		$this->load->model('Magazine_model');
	}

	// Isi peluru otomatis, jika magazine full akan berhenti mengisi
	public function autoload_post(){
		$mg_type = $this->post('magazine_type');
		$result = $this->Magazine_model->autoload($mg_type);

		if($result['is_error'] == FALSE){
			$this->response([
				'status' => true,
				'magazine_status' => $result['sta_full']
			], 200);
		}else{
			$this->response([
				'status' => false,
				'message' => $result['error_message']
			], 200);
		}
	}

	// Test magazine dengan 1 kali tembak
	public function shoot_post(){
		$mg_type = $this->post('magazine_type');
		$result = $this->Magazine_model->shootOnce($mg_type);

		if($result['is_error'] == FALSE){
			$this->response([
				'status' => true,
				'magazine_status' => $result['sta_full']
			], 200);
		}else{
			$this->response([
				'status' => false,
				'message' => $result['error_message']
			], 200);
		}
	}

	// check all magazine
	public function index_get(){
		$id = $this->get('id');

		if($id === null){
			$magazine = $this->Magazine_model->getMagazine();
		}else{
			$magazine = $this->Magazine_model->getMagazine($id);
		}

		if($magazine){
			$this->response([
				'status' => true,
				'data' => $magazine
			], 200);					// respone code for 'ok'
		}else{
			$this->response([
				'status' => false,
				'message' => 'Magazine type not found'
			], 404);					// respone code 'not found'
		}
	}

}