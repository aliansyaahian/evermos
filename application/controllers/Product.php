<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();

		$this->load->model('Product_model', 'product');
	}

	public function index_get(){
		// cek di request method GET ada parameter 'id' atau tidak
		$id = $this->get('id');

		if($id === null){
			$product = $this->product->getProduct();
		}else{
			$product = $this->product->getProduct($id);
		}
		
		if($product){
			$this->response([
				'status' => true,
				'data' => $product
			], 200);					// respone code for 'ok'
		}else{
			$this->response([
				'status' => false,
				'message' => 'Id not found'
			], 404);					// respone code 'not found'
		}
	}

}