<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Transaction extends RestController {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();

		$this->load->model('Transaction_model', 'trans');
	}

	public function index_get(){
		// cek di request method GET ada parameter 'id' atau tidak
		$id = $this->get('id');

		if($id === null){
			$transaction = $this->trans->getTransaction();
		}else{
			$transaction = $this->trans->getTransaction($id);
		}

		if($transaction){
			$this->response([
				'status' => true,
				'data' => $transaction
			], 200);					// respone code for 'ok'
		}else{
			$this->response([
				'status' => false,
				'message' => 'Transaction number not found'
			], 404);					// respone code 'not found'
		}
	}

	public function index_post(){
		$data = [
			'product_id' => $this->post('product_id'),
			'username' => $this->post('username')
		];

		$result = $this->trans->createTransaction($data);

		if($result['is_error'] == FALSE){
			$this->response([
				'status' => true,
				'data' => $data
			], 200);
		}else{
			$this->response([
				'status' => false,
				'message' => $result['error_message']
			], 200);
		}
	}
	
}